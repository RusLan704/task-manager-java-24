package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.marker.UnitCategory;

import java.util.List;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    public final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final Session session = new Session();

    @Before
    public void addUser() {
        session.setUserId("123");
        session.setSignature("345");
        session.setTimestamp(123L);
        sessionRepository.add(session);
    }

    @After
    public void removeUser() {
        sessionRepository.clearAll();
    }

    @Test
    public void testFindALL() {
        Assert.assertEquals(1, sessionRepository.findAll((session.getUserId())).size());
        sessionRepository.add(session);
        sessionRepository.add(session);
        sessionRepository.add(session);
        List<Session> sessions = sessionRepository.findAll(session.getUserId());
        Assert.assertEquals(4, sessions.size());
    }

    @Test
    public void testRemoveByUserId() {
        Assert.assertEquals(1, sessionRepository.findAll().size());
        sessionRepository.removeByUserId(session.getUserId());
        Assert.assertEquals(0, sessionRepository.findAll((session.getUserId())).size());
    }

}