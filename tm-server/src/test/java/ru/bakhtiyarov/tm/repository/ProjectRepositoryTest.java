package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.marker.UnitCategory;

import java.util.List;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private Project project;

    @Before
    public void addProject() {
        project = new Project("project", "description", "1");
        projectRepository.add(project);
    }

    @After
    public void removeProject() {
        projectRepository.clearAll();
    }

    @Test
    public void testAdd() {
        Project projectAdded = projectRepository.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(project.getName(), projectAdded.getName());
        Assert.assertEquals(project.getDescription(), projectAdded.getDescription());
        Assert.assertEquals(project.getUserId(), projectAdded.getUserId());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.add(project);
        projectRepository.add(project);
        projectRepository.add(project);
        projectRepository.clear(project.getUserId());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.remove(project);
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Project projectTest = projectRepository.removeOneByIndex(ConstTest.FALSE_USER_ID, 0);
        Assert.assertNull(projectTest);
        projectRepository.removeOneByIndex(project.getUserId(), 0);
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemoveByName() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Assert.assertNull(projectRepository.removeOneByName(ConstTest.FALSE_USER_ID, project.getName()));
        Assert.assertNull(projectRepository.removeOneByName(project.getUserId(), ConstTest.FALSE_NAME));
        projectRepository.removeOneByName(project.getUserId(), project.getName());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.removeOneById(project.getUserId(), project.getId());
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.add(project);
        projectRepository.add(project);
        projectRepository.add(project);
        projectRepository.add(project);
        final List<Project> projects = projectRepository.findAll(project.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(5, projectRepository.findAll().size());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Project projectTest = projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectTest);
        Assert.assertEquals(project.getName(), projectTest.getName());
        Assert.assertEquals(project.getUserId(), projectTest.getUserId());
        Project nullProject = projectRepository.findOneByIndex(project.getUserId(), 10);
        Assert.assertNull(nullProject);
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Project projectTest = projectRepository.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectTest);
        Assert.assertEquals(project.getUserId(), projectTest.getUserId());
        Assert.assertEquals(project.getName(), projectTest.getName());
        Project nullProject = projectRepository.findOneById(project.getUserId(), ConstTest.FALSE_ID);
        Assert.assertNull(nullProject);
    }

    @Test
    public void findOneByName() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        Project projectTest = projectRepository.findOneByName(project.getUserId(), project.getName());
        Assert.assertNotNull(projectTest);
        Assert.assertEquals(project.getUserId(), projectTest.getUserId());
        Assert.assertEquals(project.getName(), projectTest.getName());
        Project nullProject = projectRepository.findOneById(project.getUserId(), ConstTest.FALSE_NAME);
        Assert.assertNull(nullProject);
    }

}
