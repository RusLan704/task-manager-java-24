package ru.bakhtiyarov.tm.repository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public final class AbstractRepositoryTest {

    @NotNull
    private final AbstractRepository<AbstractEntityTest> abstractRepository = new AbstractRepTest();

    @NotNull
    private AbstractEntityTest abstractEntityTest;

    @Before
    public void addAbstractEntityTest() {
        abstractEntityTest = new AbstractEntityTest("entity");
        abstractRepository.add(abstractEntityTest);
    }

    @After
    public void removeAbstractEntityTest() {
        abstractRepository.clearAll();
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        abstractRepository.add(new AbstractEntityTest("1"));
        Assert.assertEquals(2, abstractRepository.findAll().size());
    }

    @Test
    public void testClearAll() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        abstractRepository.clearAll();
        Assert.assertTrue(abstractRepository.findAll().isEmpty());
    }

    @Test
    public void testAddAll() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        List<AbstractEntityTest> list = new ArrayList<>();
        list.add(abstractEntityTest);
        list.add(abstractEntityTest);
        list.add(abstractEntityTest);
        abstractRepository.addAll(list);
        Assert.assertEquals(4, abstractRepository.findAll().size());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        abstractRepository.add(abstractEntityTest);
        abstractRepository.add(abstractEntityTest);
        List<AbstractEntityTest> list = abstractRepository.findAll();
        Assert.assertEquals(3, abstractRepository.findAll().size());
    }

    @Test
    public void testFindBYIdTest() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        AbstractEntityTest abstractEntityTest = abstractRepository.findById(this.abstractEntityTest.getId());
        Assert.assertNotNull(abstractEntityTest);
        Assert.assertEquals(abstractEntityTest.getId(), this.abstractEntityTest.getId());
        AbstractEntityTest abstractEntityTest2 = abstractRepository.findById(ConstTest.FALSE_ID);
        Assert.assertNull(abstractEntityTest2);
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        abstractRepository.remove(abstractEntityTest);
        Assert.assertEquals(0, abstractRepository.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(1, abstractRepository.findAll().size());
        AbstractEntityTest abstractEntityTest1 = abstractRepository.removeById(abstractEntityTest.getId());
        Assert.assertNotNull(abstractEntityTest1);
        Assert.assertEquals(0, abstractRepository.findAll().size());
        AbstractEntityTest abstractEntityTest2 = abstractRepository.removeById(ConstTest.FALSE_ID);
        Assert.assertNull(abstractEntityTest2);
    }

    private static final class AbstractRepTest extends AbstractRepository<AbstractEntityTest> {
    }

    @Getter
    @Setter
    @AllArgsConstructor
    private static final class AbstractEntityTest extends AbstractEntity {

        @Nullable
        private String name;

    }

}
