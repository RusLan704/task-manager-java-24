package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.empty.*;
import ru.bakhtiyarov.tm.marker.UnitCategory;
import ru.bakhtiyarov.tm.repository.UserRepository;
import ru.bakhtiyarov.tm.util.HashUtil;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final User user = new User("login", "password", "email");

    @Before
    public void addUser() {
        userRepository.add(user);
    }

    @After
    public void removeUser() {
        userRepository.clearAll();
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(1, userService.findAll().size());
        userService.removeByLogin(user.getLogin());
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testCreateLoginPasswordEmail() {
        Assert.assertEquals(1, userService.findAll().size());
        User newUser = userService.create("login", "password", "email");
        Assert.assertEquals(2, userService.findAll().size());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(newUser.getLogin(), "login");
        Assert.assertEquals(newUser.getEmail(), "email");
        Assert.assertEquals(newUser.getRole(), Role.USER);
    }

    @Test
    public void testCreateLoginPassword() {
        Assert.assertEquals(1, userService.findAll().size());
        User newUser = userService.create("login", "password");
        Assert.assertNotNull(newUser);
        Assert.assertEquals(2, userService.findAll().size());
        Assert.assertEquals(newUser.getLogin(), "login");
        Assert.assertEquals(newUser.getRole(), Role.USER);
    }

    @Test
    public void testCreateLoginPasswordRole() {
        Assert.assertEquals(1, userService.findAll().size());
        User user = userService.create("login", "password", Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(2, userService.findAll().size());
        Assert.assertEquals(user.getLogin(), "login");
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test
    public void testUpdatePassword() {
        User nullUser = userService.updatePassword(ConstTest.FALSE_USER_ID, "new pass");
        Assert.assertNull(nullUser);
        User testUser = userService.updatePassword(user.getId(), "new pass");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(HashUtil.salt("new pass"), user.getPasswordHash());
    }

    @Test
    public void testUpdateUserEmail() {
        User nullUser = userService.updateUserEmail(ConstTest.FALSE_USER_ID, "email");
        Assert.assertNull(nullUser);
        User testUser = userService.updateUserEmail(user.getId(), "new email");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getEmail(), "new email");
    }

    @Test
    public void testUpdateUserLogin() {
        User nullUser = userService.updateUserLogin(ConstTest.FALSE_USER_ID, "login");
        Assert.assertNull(nullUser);
        User testUser = userService.updateUserLogin(user.getId(), "new login");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getLogin(), "new login");
    }

    @Test
    public void testUpdateFirstName() {
        User nullUser = userService.updateUserFirstName(ConstTest.FALSE_USER_ID, "first name");
        Assert.assertNull(nullUser);
        User testUser = userService.updateUserFirstName(user.getId(), "new first name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getFirstName(), "new first name");
    }

    @Test
    public void testUpdateLastName() {
        User nullUser = userService.updateUserLastName(ConstTest.FALSE_USER_ID, "last name");
        Assert.assertNull(nullUser);
        User testUser = userService.updateUserLastName(user.getId(), "new last name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getLastName(), "new last name");
    }

    @Test
    public void testUpdateMiddleName() {
        User nullUser = userService.updateUserMiddleName(ConstTest.FALSE_USER_ID, "mid name");
        Assert.assertNull(nullUser);
        User testUser = userService.updateUserMiddleName(user.getId(), "new middle name");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getMiddleName(), "new middle name");
    }

    @Test
    public void testFindByLogin() {
        User testUser = userService.findByLogin(user.getLogin());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getId(), user.getId());
        Assert.assertEquals(testUser.getLogin(), user.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        Assert.assertEquals(user.getLocked(), false);
        userService.lockUserByLogin(user.getLogin());
        Assert.assertEquals(user.getLocked(), true);
    }

    @Test
    public void testUnLockedUserByLogin() {
        userService.lockUserByLogin(user.getLogin());
        Assert.assertEquals(user.getLocked(), true);
        userService.unLockUserByLogin(user.getLogin());
        Assert.assertEquals(user.getLocked(), false);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRemoveWithEmptyLogin() {
        userService.removeByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRemoveWithNullLogin() {
        userService.removeByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithEmailWithEmptyLogin() {
        userService.create("", "password", "email");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithEmailWithNullLogin() {
        userService.create(null, "password", "email");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithEmailWithEmptyPassword() {
        userService.create("login", "", "email");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithEmailWithNullPassword() {
        userService.create("login", null, "email");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeCreateWithEmailWithEmptyEmail() {
        userService.create("login", "password", "");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeCreateWithEmailWithNullEmail() {
        userService.create("login", "password", (String) null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithEmptyLogin() {
        userService.create("", "password");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithNullLogin() {
        userService.create(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithEmptyPassword() {
        userService.create("login", "");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithNullPassword() {
        userService.create("login", null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithRoleWithEmptyLogin() {
        userService.create("", "password", Role.USER);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithRoleWithNullLogin() {
        userService.create(null, "password", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithRoleWithEmptyPassword() {
        userService.create("login", "", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithRoleWithNullPassword() {
        userService.create("login", null, Role.USER);
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeCreateWithRoleWithNullRole() {
        userService.create("login", "password", (Role) null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdatePasswordWithEmptyId() {
        userService.updatePassword("", "password");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdatePasswordWithNullId() {
        userService.updatePassword(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeUpdatePasswordWithEmptyPassword() {
        userService.updatePassword("id", "");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeUpdatePasswordWithNullPassword() {
        userService.updatePassword("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateEmailWithEmptyId() {
        userService.updateUserEmail("", "email");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateEmailWithNullId() {
        userService.updateUserEmail(null, "email");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeUpdateEmailWithEmptyEmail() {
        userService.updateUserEmail("id", "");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeUpdateEmailWithNullEmail() {
        userService.updateUserEmail("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateFirstNameWithEmptyId() {
        userService.updateUserFirstName("", "first name");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateFirstNameWithNullId() {
        userService.updateUserFirstName(null, "first name");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeUpdateFirstNameWithEmptyFirstName() {
        userService.updateUserFirstName("id", "");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeUpdateFirstNameWithNullFirstName() {
        userService.updateUserFirstName("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLastNameWithEmptyId() {
        userService.updateUserLastName("", "last name");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLastNameWithNullId() {
        userService.updateUserLastName(null, "last name");
    }

    @Test(expected = EmptyLastNameException.class)
    public void testNegativeUpdateLastNameWithEmptyLastName() {
        userService.updateUserLastName("id", "");
    }

    @Test(expected = EmptyLastNameException.class)
    public void testNegativeUpdateLastNameWithNullLastName() {
        userService.updateUserLastName("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateMiddleNameWithEmptyId() {
        userService.updateUserMiddleName("", "middle name");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateMiddleNameWithNullId() {
        userService.updateUserMiddleName(null, "middle name");
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void testNegativeUpdateMiddleNameWithEmptyLastName() {
        userService.updateUserMiddleName("id", "");
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void testNegativeUpdateMiddleNameWithNullLastName() {
        userService.updateUserMiddleName("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLoginWithEmptyId() {
        userService.updateUserLogin("", "login");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLoginWithNullId() {
        userService.updateUserLogin(null, "login");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUpdateLoginWithEmptyLastName() {
        userService.updateUserLogin("id", "");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUpdateLoginWithNullLastName() {
        userService.updateUserLogin("id", null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeLockUserByLoginWithEmptyLogin() {
        userService.lockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeLockUserByLoginWithNullLogin() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUnLockUserByLoginWithEmptyLogin() {
        userService.unLockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUnLockUserByLoginWithNullLogin() {
        userService.unLockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeFindByLoginWithEmptyLogin() {
        userService.findByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeFindUserByLoginWithNullLogin() {
        userService.findByLogin(null);
    }

}