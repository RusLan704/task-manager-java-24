package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.marker.UnitCategory;
import ru.bakhtiyarov.tm.repository.ProjectRepository;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void testCreateUserIdName() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create("1", "name");
        Assert.assertEquals(1, projectService.findAll().size());
        Project project = projectService.findOneByName("1", "name");
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), "name");
        Assert.assertEquals(project.getUserId(), "1");
    }

    @Test
    public void testCreateUserIdNameDescription() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create("1", "name", "234");
        Assert.assertEquals(1, projectService.findAll().size());
        Project project = projectService.findOneByName("1", "name");
        Assert.assertNotNull(project);
        Assert.assertEquals("name", project.getName());
        Assert.assertEquals("234", project.getDescription());
        Assert.assertEquals("1", project.getUserId());
    }

    @Test
    public void testAdd() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Project project = new Project("1234", "2");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Project project = new Project("1234", "2");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.remove(project.getUserId(), project);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void testFindAll() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Project project = new Project("1234", "2");
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(4, projectService.findAll(project.getUserId()).size());
    }

    @Test
    public void clearAll() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Project project = new Project("1234", "2");
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(4, projectService.findAll(project.getUserId()).size());
        projectService.clearAll();
        Assert.assertTrue(projectService.findAll(project.getUserId()).isEmpty());
    }

    @Test
    public void testFindOneByName() {
        Project project = new Project("project", "2231");
        projectService.add(project.getUserId(), project);
        Project tempProject = projectService.findOneByName(project.getUserId(), project.getName());
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
    }

    @Test
    public void testFindOneByIndex() {
        Project project = new Project("project", "2231");
        projectService.add(project.getUserId(), project);
        Project tempProject = projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
    }

    @Test
    public void testFindOneById() {
        Project project = new Project("project", "2231");
        projectService.add(project.getUserId(), project);
        Project tempProject = projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
    }

    @Test
    public void testRemoveOneByIndex() {
        Project project = new Project("project", "2231");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll(project.getUserId()).size());
        projectService.removeOneByIndex(project.getUserId(), 0);
        Assert.assertEquals(0, projectService.findAll(project.getUserId()).size());
    }

    @Test
    public void testRemoveOneById() {
        Project project = new Project("project", "2231");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll(project.getUserId()).size());
        projectService.removeOneById(project.getUserId(), project.getId());
        Assert.assertEquals(0, projectService.findAll(project.getUserId()).size());
    }

    @Test
    public void testRemoveOneByName() {
        Project project = new Project("project", "2231");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll(project.getUserId()).size());
        projectService.removeOneByName(project.getUserId(), project.getName());
        Assert.assertEquals(0, projectService.findAll(project.getUserId()).size());
    }

    @Test
    public void testUpdateTakByIndex() {
        Project project = new Project("name", "description", "123");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll(project.getUserId()).size());
        Project tempProject = projectService.updateProjectByIndex(project.getUserId(), 0, "new name", "new description");
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(tempProject.getId(), project.getId());
        Assert.assertEquals(tempProject.getName(), "new name");
        Assert.assertEquals(tempProject.getDescription(), "new description");
    }

    @Test
    public void testUpdateProjectById() {
        Project project = new Project("name", "description", "123");
        projectService.add(project.getUserId(), project);
        Assert.assertEquals(1, projectService.findAll(project.getUserId()).size());
        Project tempProject = projectService.updateProjectById(project.getUserId(), project.getId(), "new name", "new description");
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(tempProject.getId(), project.getId());
        Assert.assertEquals(tempProject.getName(), "new name");
        Assert.assertEquals(tempProject.getDescription(), "new description");
        Project nullProject = projectService.updateProjectById("1234", project.getId(), "new name", "new description");
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithEmptyUserId() {
        projectService.create("", "name");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithNullUserId() {
        projectService.create(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithEmptyName() {
        projectService.create("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithNullName() {
        projectService.create("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithEmptyUserId() {
        projectService.create("", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithNullUserId() {
        projectService.create(null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithEmptyName() {
        projectService.create("123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithNullName() {
        projectService.create("123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithEmptyDesc() {
        projectService.create("123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithNullDesc() {
        projectService.create("123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithEmptyUserId() {
        projectService.add("", new Project());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithNullUserId() {
        projectService.add(null, new Project());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithEmptyUserId() {
        projectService.remove("", new Project());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithNullUserId() {
        projectService.remove(null, new Project());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithEmptyUserId() {
        projectService.findAll("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithNullUserId() {
        projectService.findAll(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithEmptyUserId() {
        projectService.clear("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithNullUserId() {
        projectService.clear(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithEmptyUserId() {
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithNullUserId() {
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithMinusIndex() {
        projectService.removeOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithNullIndex() {
        projectService.removeOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithIncorrectIndex() {
        Project project = new Project("name", "123");
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.removeOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyUserId() {
        projectService.removeOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithNullUserId() {
        projectService.removeOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyId() {
        projectService.removeOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithNullId() {
        projectService.removeOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneNameIdWithEmptyUserId() {
        projectService.removeOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithNullUserId() {
        projectService.removeOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneNameIdWithEmptyName() {
        projectService.removeOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithNullName() {
        projectService.removeOneByName("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyUserId() {
        projectService.updateProjectById("", "123", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIdWithNullUserId() {
        projectService.updateProjectById(null, "123", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyId() {
        projectService.updateProjectById("123", "", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateProjectByIdWithNullId() {
        projectService.updateProjectById("123", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyName() {
        projectService.updateProjectById("123", "123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIdWithNullName() {
        projectService.updateProjectById("123", "123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIdIdWithEmptyDesc() {
        projectService.updateProjectById("123", "123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIdWithNullDesc() {
        projectService.updateProjectById("123", "123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIndexIdWithEmptyUserId() {
        projectService.updateProjectByIndex("", 0, "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIndexWithNullUserId() {
        projectService.updateProjectByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexIdWithMinusId() {
        projectService.updateProjectByIndex("123", -10, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexWithNullId() {
        projectService.updateProjectByIndex("123", null, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexWithIncorrectId() {
        Project project = new Project("name", "123");
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.updateProjectByIndex("123", 2, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIndexIdWithEmptyName() {
        projectService.updateProjectByIndex("123", 0, "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIndexWithNullName() {
        projectService.updateProjectByIndex("123", 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIndexIdWithEmptyDesc() {
        projectService.updateProjectByIndex("123", 0, "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIndexWithNullDesc() {
        projectService.updateProjectByIndex("123", 0, "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithEmptyUserId() {
        projectService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithNullUserId() {
        projectService.findOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithEmptyId() {
        projectService.findOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithNullId() {
        projectService.findOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithEmptyUserId() {
        projectService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithNullUserId() {
        projectService.findOneById(null, "123");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithMinusUserIndex() {
        projectService.findOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithNullIndex() {
        projectService.findOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithIncorrectIndex() {
        Project project = new Project("name", "123");
        projectService.add(project.getUserId(), project);
        projectService.add(project.getUserId(), project);
        projectService.findOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithEmptyUserId() {
        projectService.findOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameIdWithNullUserId() {
        projectService.findOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithEmptyName() {
        projectService.findOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameIdWithNullName() {
        projectService.findOneByName("123", null);
    }

}
