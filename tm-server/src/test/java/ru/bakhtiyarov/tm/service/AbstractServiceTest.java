package ru.bakhtiyarov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.entity.AbstractEntity;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.marker.UnitCategory;
import ru.bakhtiyarov.tm.repository.AbstractRepository;

import java.util.Arrays;
import java.util.List;

@Category(UnitCategory.class)
public class AbstractServiceTest {

    @NotNull
    private final AbstractRepository<AbstractEntityTest> abstractRepository = new AbstractRepositoryTest();

    @NotNull
    private final AbstractService<AbstractEntityTest> abstractService = new AbstractServiceTestable(abstractRepository);

    @Test
    public void testAddAll() {
        Assert.assertEquals(0, abstractService.findAll().size());
        List<AbstractEntityTest> lists = Arrays.asList
                (
                        new AbstractEntityTest("name"),
                        new AbstractEntityTest("name"),
                        new AbstractEntityTest("name")
                );
        abstractService.addAll(lists);
        Assert.assertEquals(3, abstractService.findAll().size());
    }

    @Test
    public void testClearAll() {
        Assert.assertEquals(0, abstractService.findAll().size());
        List<AbstractEntityTest> lists = Arrays.asList
                (
                        new AbstractEntityTest("name"),
                        new AbstractEntityTest("name"),
                        new AbstractEntityTest("name")
                );
        abstractService.addAll(lists);
        Assert.assertEquals(3, abstractService.findAll().size());
        abstractService.clearAll();
        Assert.assertEquals(0, abstractService.findAll().size());
    }

    @Test
    public void testFindById() {
        Assert.assertEquals(0, abstractService.findAll().size());
        AbstractEntityTest abstractEntityTest = new AbstractEntityTest("name");
        abstractRepository.add(abstractEntityTest);
        AbstractEntityTest tempAbstractEntityTest = abstractService.findById(abstractEntityTest.getId());
        Assert.assertNotNull(tempAbstractEntityTest);
        Assert.assertEquals(abstractEntityTest.getId(), tempAbstractEntityTest.getId());
        Assert.assertEquals(abstractEntityTest.getName(), tempAbstractEntityTest.getName());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(0, abstractService.findAll().size());
        AbstractEntityTest abstractEntityTest = new AbstractEntityTest("name");
        abstractRepository.add(abstractEntityTest);
        Assert.assertEquals(1, abstractService.findAll().size());
        AbstractEntityTest tempAbstractEntityTest = abstractService.removeById(abstractEntityTest.getId());
        Assert.assertEquals(0, abstractService.findAll().size());
        Assert.assertNotNull(tempAbstractEntityTest);
        Assert.assertEquals(abstractEntityTest.getId(), tempAbstractEntityTest.getId());
        Assert.assertEquals(abstractEntityTest.getName(), tempAbstractEntityTest.getName());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindByIdWithEmptyId() {
        abstractService.findById("");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindByIdWithNullId() {
        abstractService.findById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveByIdWithEmptyId() {
        abstractService.removeById("");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveByIdWithNullId() {
        abstractService.removeById(null);
    }

    private static final class AbstractRepositoryTest extends AbstractRepository<AbstractEntityTest> {
    }

    private static final class AbstractServiceTestable extends AbstractService<AbstractEntityTest> {

        public AbstractServiceTestable(@NotNull final IRepository<AbstractEntityTest> repository) {
            super(repository);
        }

    }

    @Getter
    @Setter
    @AllArgsConstructor
    private static final class AbstractEntityTest extends AbstractEntity {

        @Nullable
        private String name;

    }

}
