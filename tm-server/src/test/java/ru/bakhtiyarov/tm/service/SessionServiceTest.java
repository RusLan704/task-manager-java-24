package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.marker.UnitCategory;
import ru.bakhtiyarov.tm.repository.SessionRepository;

import java.util.List;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @Before
    public void addUser() {
        serviceLocator.getUserService().create("test", "test", "mail.ru");
    }

    @After
    public void removeUser() {
        serviceLocator.getUserService().removeByLogin("login");
    }

    @Test
    public void testOpen() {
        @Nullable final Session session = sessionService.open("test", "test");
        Assert.assertNotNull(session);
        @Nullable final Session testSession = sessionRepository.findById(session.getId());
        Assert.assertNotNull(testSession);
        Assert.assertEquals(session.getId(), testSession.getId());
    }

    @Test
    public void testCheckDataAccess() {
        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        Assert.assertFalse(sessionService.checkDataAccess(ConstTest.FALSE_LOGIN, "test"));
        Assert.assertFalse(sessionService.checkDataAccess("test", ConstTest.FALSE_PASSWORD));
        Assert.assertFalse(sessionService.checkDataAccess(ConstTest.FALSE_LOGIN, ConstTest.FALSE_PASSWORD));
    }

    @Test
    public void testSign() {
        @Nullable final Session session = new Session();
        Assert.assertNotNull(sessionService.sign(session));
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    public void testIsValid() {
        @Nullable final Session session = sessionService.open("test", "test");
        Assert.assertNotNull(session);
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertFalse(sessionService.isValid(null));
        Assert.assertFalse(sessionService.isValid(new Session()));
    }

    @Test
    public void testSignOutByUserId() {
        @Nullable final User user = serviceLocator.getUserService().findByLogin("test");
        Assert.assertNotNull(user);
        sessionService.open(user.getLogin(), "test");
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.signOutByUserId(user.getId());
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testSignOutByLogin() {
        @Nullable final User user = serviceLocator.getUserService().findByLogin("test");
        Assert.assertNotNull(user);
        sessionService.open(user.getLogin(), "test");
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.signOutByLogin(user.getLogin());
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testClose() {
        @Nullable Session session = sessionService.open("test", "test");
        Assert.assertNotNull(session);
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.close(session);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testCloseAll() {
        @Nullable Session session = sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        Assert.assertNotNull(session);
        Assert.assertEquals(4, sessionService.findAll().size());
        sessionService.closeAll(session);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testGetUser() {
        @Nullable Session session = sessionService.open("test", "test");
        @Nullable final User user = sessionService.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
    }

    @Test
    public void testGetUserId() {
        @Nullable Session session = sessionService.open("test", "test");
        @Nullable final String userId = sessionService.getUserId(session);
        Assert.assertNotNull(userId);
        Assert.assertEquals(session.getUserId(), userId);
    }

    @Test
    public void testGetListenSession() {
        @Nullable Session session = sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        sessionService.open("test", "test");
        List<Session> sessions = sessionService.getListSession(session);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(4, sessions.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeOpenWithFalseCheck() {
        sessionService.open(ConstTest.FALSE_LOGIN, "test");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeOpenWithLockUser() {
        @Nullable final User user = serviceLocator.getUserService().findByLogin("test");
        user.setLocked(true);
        sessionService.open("test", "test");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithEmptyLogin() {
        sessionService.checkDataAccess("", "password");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithNullLogin() {
        sessionService.checkDataAccess(null, "password");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithEmptyPassword() {
        sessionService.checkDataAccess("test", "");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCheckDataAccessWithNullPassword() {
        sessionService.checkDataAccess("test", null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullSession() {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithEmptySignature() {
        Session session = new Session();
        session.setSignature("");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullSignature() {
        Session session = new Session();
        session.setSignature(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithEmptyUserId() {
        Session session = new Session();
        session.setUserId("");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullUserId() {
        Session session = new Session();
        session.setUserId(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithNullTimeStamp() {
        Session session = new Session();
        session.setTimestamp(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithUnequalSignature() {
        Session session = new Session();
        session.setUserId("123");
        session.setTimestamp(123L);
        session.setSignature("321");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithNullRole() {
        sessionService.validate(new Session(), null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithNullUser() {
        sessionService.validate(new Session(), Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithNullUserRole() {
        @Nullable final User user = serviceLocator.getUserService().findByLogin("test");
        user.setRole(null);
        sessionService.validate(new Session(), Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeValidateWithRoleWithUnequalUserRole() {
        @Nullable final User user = serviceLocator.getUserService().findByLogin("test");
        user.setRole(Role.USER);
        sessionService.validate(new Session(), Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByLoginWithEmptyLogin() {
        sessionService.signOutByLogin("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByLoginWithNullLogin() {
        sessionService.signOutByLogin(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByLoginWithIncorrectLogin() {
        sessionService.signOutByLogin(ConstTest.FALSE_LOGIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByUserIdWithEmptyUserId() {
        sessionService.signOutByUserId("");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeSignOutByUserIdWithNullUserId() {
        sessionService.signOutByUserId(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCloseWithNullSession() {
        sessionService.close(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeCloseAllWithNullSession() {
        sessionService.closeAll(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeGetUserWithNullSession() {
        sessionService.getUser(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeGetUserWithIncorrectSession() {
        sessionService.getUser(new Session());
    }

    @Test(expected = AccessDeniedException.class)
    public void testNegativeGetUserIdWithNullSession() {
        sessionService.getUserId(null);
    }

}
