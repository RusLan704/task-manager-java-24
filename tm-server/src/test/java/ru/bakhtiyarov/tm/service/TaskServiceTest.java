package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.marker.UnitCategory;
import ru.bakhtiyarov.tm.repository.TaskRepository;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Test
    public void testCreateUserIdName() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create("1", "name");
        Assert.assertEquals(1, taskService.findAll().size());
        Task task = taskService.findOneByName("1", "name");
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), "name");
        Assert.assertEquals(task.getUserId(), "1");
    }

    @Test
    public void testCreateUserIdNameDescription() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create("1", "name", "234");
        Assert.assertEquals(1, taskService.findAll().size());
        Task task = taskService.findOneByName("1", "name");
        Assert.assertNotNull(task);
        Assert.assertEquals("name", task.getName());
        Assert.assertEquals("234", task.getDescription());
        Assert.assertEquals("1", task.getUserId());
    }

    @Test
    public void testAdd() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Task task = new Task("1234", "2");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Task task = new Task("1234", "2");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.remove(task.getUserId(), task);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void testFindAll() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Task task = new Task("1234", "2");
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(4, taskService.findAll(task.getUserId()).size());
    }

    @Test
    public void clearAll() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Task task = new Task("1234", "2");
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(4, taskService.findAll(task.getUserId()).size());
        taskService.clearAll();
        Assert.assertTrue(taskService.findAll(task.getUserId()).isEmpty());
    }

    @Test
    public void testFindOneByName() {
        Task task = new Task("task", "2231");
        taskService.add(task.getUserId(), task);
        Task tempTask = taskService.findOneByName(task.getUserId(), task.getName());
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(task.getId(), tempTask.getId());
    }

    @Test
    public void testFindOneByIndex() {
        Task task = new Task("task", "2231");
        taskService.add(task.getUserId(), task);
        Task tempTask = taskService.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(task.getId(), tempTask.getId());
    }

    @Test
    public void testFindOneById() {
        Task task = new Task("task", "2231");
        taskService.add(task.getUserId(), task);
        Task tempTask = taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(task.getId(), tempTask.getId());
    }

    @Test
    public void testRemoveOneByIndex() {
        Task task = new Task("task", "2231");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll(task.getUserId()).size());
        taskService.removeOneByIndex(task.getUserId(), 0);
        Assert.assertEquals(0, taskService.findAll(task.getUserId()).size());
    }

    @Test
    public void testRemoveOneById() {
        Task task = new Task("task", "2231");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll(task.getUserId()).size());
        taskService.removeOneById(task.getUserId(), task.getId());
        Assert.assertEquals(0, taskService.findAll(task.getUserId()).size());
    }

    @Test
    public void testRemoveOneByName() {
        Task task = new Task("task", "2231");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll(task.getUserId()).size());
        taskService.removeOneByName(task.getUserId(), task.getName());
        Assert.assertEquals(0, taskService.findAll(task.getUserId()).size());
    }

    @Test
    public void testUpdateTakByIndex() {
        Task task = new Task("name", "description", "123");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll(task.getUserId()).size());
        Task tempTask = taskService.updateTaskByIndex(task.getUserId(), 0, "new name", "new description");
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(tempTask.getId(), task.getId());
        Assert.assertEquals(tempTask.getName(), "new name");
        Assert.assertEquals(tempTask.getDescription(), "new description");
    }

    @Test
    public void testUpdateTaskById() {
        Task task = new Task("name", "description", "123");
        taskService.add(task.getUserId(), task);
        Assert.assertEquals(1, taskService.findAll(task.getUserId()).size());
        Task tempTask = taskService.updateTaskById(task.getUserId(), task.getId(), "new name", "new description");
        Assert.assertNotNull(tempTask);
        Assert.assertEquals(tempTask.getId(), task.getId());
        Assert.assertEquals(tempTask.getName(), "new name");
        Assert.assertEquals(tempTask.getDescription(), "new description");
        Task nullTask = taskService.updateTaskById("1234", task.getId(), "new name", "new description");
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithEmptyUserId() {
        taskService.create("", "name");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithNullUserId() {
        taskService.create(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithEmptyName() {
        taskService.create("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithNullName() {
        taskService.create("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithEmptyUserId() {
        taskService.create("", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithNullUserId() {
        taskService.create(null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithEmptyName() {
        taskService.create("123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithNullName() {
        taskService.create("123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithEmptyDesc() {
        taskService.create("123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithNullDesc() {
        taskService.create("123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithEmptyUserId() {
        taskService.add("", new Task());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithNullUserId() {
        taskService.add(null, new Task());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithEmptyUserId() {
        taskService.remove("", new Task());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithNullUserId() {
        taskService.remove(null, new Task());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithEmptyUserId() {
        taskService.findAll("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithNullUserId() {
        taskService.findAll(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithEmptyUserId() {
        taskService.clear("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithNullUserId() {
        taskService.clear(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithEmptyUserId() {
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithNullUserId() {
        taskService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithMinusIndex() {
        taskService.removeOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithNullIndex() {
        taskService.removeOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithIncorrectIndex() {
        Task task = new Task("name", "123");
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.removeOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyUserId() {
        taskService.removeOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithNullUserId() {
        taskService.removeOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyId() {
        taskService.removeOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithNullId() {
        taskService.removeOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneNameIdWithEmptyUserId() {
        taskService.removeOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithNullUserId() {
        taskService.removeOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneNameIdWithEmptyName() {
        taskService.removeOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithNullName() {
        taskService.removeOneByName("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyUserId() {
        taskService.updateTaskById("", "123", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIdWithNullUserId() {
        taskService.updateTaskById(null, "123", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyId() {
        taskService.updateTaskById("123", "", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateTaskByIdWithNullId() {
        taskService.updateTaskById("123", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyName() {
        taskService.updateTaskById("123", "123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIdWithNullName() {
        taskService.updateTaskById("123", "123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyDesc() {
        taskService.updateTaskById("123", "123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIdWithNullDesc() {
        taskService.updateTaskById("123", "123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIndexIdWithEmptyUserId() {
        taskService.updateTaskByIndex("", 0, "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIndexWithNullUserId() {
        taskService.updateTaskByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexIdWithMinusId() {
        taskService.updateTaskByIndex("123", -10, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexWithNullId() {
        taskService.updateTaskByIndex("123", null, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexWithIncorrectId() {
        Task task = new Task("name", "123");
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.updateTaskByIndex("123", 2, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIndexIdWithEmptyName() {
        taskService.updateTaskByIndex("123", 0, "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIndexWithNullName() {
        taskService.updateTaskByIndex("123", 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIndexIdWithEmptyDesc() {
        taskService.updateTaskByIndex("123", 0, "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIndexWithNullDesc() {
        taskService.updateTaskByIndex("123", 0, "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithEmptyUserId() {
        taskService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithNullUserId() {
        taskService.findOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithEmptyId() {
        taskService.findOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithNullId() {
        taskService.findOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithEmptyUserId() {
        taskService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithNullUserId() {
        taskService.findOneById(null, "123");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithMinusUserIndex() {
        taskService.findOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithNullIndex() {
        taskService.findOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithIncorrectIndex() {
        Task task = new Task("name", "123");
        taskService.add(task.getUserId(), task);
        taskService.add(task.getUserId(), task);
        taskService.findOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithEmptyUserId() {
        taskService.findOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameIdWithNullUserId() {
        taskService.findOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithEmptyName() {
        taskService.findOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameIdWithNullName() {
        taskService.findOneByName("123", null);
    }

}