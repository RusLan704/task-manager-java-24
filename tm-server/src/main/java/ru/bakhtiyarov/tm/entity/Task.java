package ru.bakhtiyarov.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements Serializable {

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String userId;

    @NotNull
    public Task(@NotNull String name, @NotNull String description, @NotNull String userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    @NotNull
    public Task(@NotNull String name, @Nullable String userId) {
        this.name = name;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
