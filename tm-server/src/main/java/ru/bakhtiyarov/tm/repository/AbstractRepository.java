package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public void clearAll() {
        entities.clear();
    }

    @Override
    public void addAll(@NotNull List<E> records) {
        this.entities.addAll(records);
    }

    @Override
    public E removeById(@Nullable String id) {
        final @Nullable E entity = findById(id);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E findById(@NotNull String id) {
        for (@NotNull final E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E remove(@NotNull E record) {
        entities.remove(record);
        return record;
    }

    @NotNull
    @Override
    public E add(@NotNull E record) {
        entities.add(record);
        return record;
    }

}