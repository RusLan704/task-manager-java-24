package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAll(userId);
        this.entities.removeAll(tasks);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@Nullable final Task task : entities) {
            if (task == null) continue;
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        if (!userId.equals(task.getUserId())) return;
        entities.remove(task);
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        for (@NotNull final Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (entities.indexOf(task) == index) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final Task task : entities) {
            if (task == null) continue;
            if (userId.equals(task.getUserId()) && task.getId().equals(id))
                return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final Task task : entities) {
            if (task == null) continue;
            if (userId.equals(task.getUserId()) && task.getName().equals(name))
                return task;
        }
        return null;
    }

}
