package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.User;

import java.util.List;

public interface  IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    User removeById(@NotNull String id);

}
