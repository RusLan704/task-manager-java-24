package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(String login, String password);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    Session sign(@Nullable Session session);

    @Nullable
    User getUser(@Nullable Session session);

    @Nullable
    String getUserId(@Nullable Session session);

    @Nullable
    List<Session> getListSession(@Nullable Session session);

    void close(@Nullable Session session);

    boolean closeAll(@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role);

    void validate(@Nullable Session session);

    boolean isValid(@Nullable Session session);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userID);

}
