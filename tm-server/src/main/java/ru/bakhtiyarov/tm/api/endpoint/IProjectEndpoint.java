package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    void createProjectByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    void addAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "projects", partName = "projects") final List<Project> project
    );

    @WebMethod
    void addProject(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "project", partName = "project") final Project project
    );

    @NotNull
    @WebMethod
    List<Project> findAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void clearAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void clearAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    List<Project> findAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @Nullable
    @WebMethod
    Project findProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    Project findProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    Project findProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    Project removeProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    Project removeProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    Project removeProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    Project updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @Nullable
    @WebMethod
    Project updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

}
