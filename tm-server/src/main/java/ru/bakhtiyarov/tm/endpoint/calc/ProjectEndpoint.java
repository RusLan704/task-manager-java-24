package ru.bakhtiyarov.tm.endpoint.calc;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.endpoint.AbstractEndpoint;

public class ProjectEndpoint extends AbstractEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public ProjectEndpoint(){
        super(null);
    }




}
