package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.ITaskEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void addAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "tasks", partName = "tasks") final List<Task> tasks
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().addAll(tasks);
    }

    @Override
    @WebMethod
    public void addTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        String  userId = session.getUserId();
        if(userId == null) return;
        serviceLocator.getTaskService().add(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void createTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @Override
    public void createTaskByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void clearAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> finaAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll();
    }


    @Override
    @WebMethod
    public void clearAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clearAll();
    }

}
