package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IUserEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    public UserEndpoint() {
        super(null);
    }

    @NotNull
    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public User createUserByLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password);
    }

    @Nullable
    @Override
    public User createUserByLoginPasswordEmail(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {
        return serviceLocator.getUserService().create(login, password, email);
    }


    @Nullable
    @Override
    public User createUserByLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final Role role
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @NotNull
    @Override
    public List<User> findAllUsers(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findAll();
    }

    @Nullable
    @Override
    public User findUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Nullable
    public User findUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

    @Nullable
    @Override
    public User updateUserPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updatePassword(session.getUserId(), password);
    }

    @Nullable
    @Override
    public User updateUserEmail(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserEmail(session.getUserId(), email);
    }

    @Nullable
    @Override
    public User updateUserFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") final String firstName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserFirstName(session.getUserId(), firstName);
    }

    @Nullable
    @Override
    public User updateUserLastName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "lastName", partName = "lastName") final String lastName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserLastName(session.getUserId(), lastName);
    }

    @Nullable
    @Override
    public User updateUserMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "middleName", partName = "middleName") final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserMiddleName(session.getUserId(), middleName);
    }

    @Nullable
    @Override
    public User updateUserLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserLogin(session.getUserId(), login);
    }

}
