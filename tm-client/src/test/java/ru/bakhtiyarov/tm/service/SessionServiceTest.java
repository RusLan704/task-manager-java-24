package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private final ISessionService sessionService = new SessionService();

    @Test
    public void testGetAndSetSession(){
        sessionService.setSession(new Session());
        Assert.assertNotNull(sessionService.getSession());
    }

}
