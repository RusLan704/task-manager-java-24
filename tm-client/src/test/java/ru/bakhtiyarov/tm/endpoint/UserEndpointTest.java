package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.marker.IntegrationCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

@Category(IntegrationCategory.class)
public class UserEndpointTest {

    @NotNull
    private static final EndpointLocator endpointLocator = new Bootstrap();

    @NotNull
    private static UserEndpoint userEndpoint;

    @NotNull
    private static AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private static SessionEndpoint sessionEndpoint;

    @NotNull
    private static Session session;

    @BeforeClass
    public static void initData() {
        userEndpoint = endpointLocator.getUserEndpoint();
        adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();
        session = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @AfterClass
    public static void closeSession() {
        sessionEndpoint.closeSessionAll(session);
    }

    @Test
    public void testCreateUserByLoginPassword() {
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
        userEndpoint.createUserByLoginPassword(session, "test2", "test333");
        Assert.assertEquals(3, userEndpoint.findAllUsers(session).size());
        adminUserEndpoint.removeUserByLogin(session, "test2");
    }

    @Test
    public void testCreateUserByEmail() {
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
        userEndpoint.createUserByLoginPasswordEmail("test3", "test333", "test333@mail.ru");
        Assert.assertEquals(3, userEndpoint.findAllUsers(session).size());
        adminUserEndpoint.removeUserByLogin(session, "test3");
    }

    @Test
    public void testUpdatePassword() {
        @NotNull User user = userEndpoint.findUserById(session);
        @NotNull User userTest = userEndpoint.updateUserPassword(session, "pass");
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), userTest.getId());
        Assert.assertNotEquals(user.getPasswordHash(), userTest.getPasswordHash());
        userEndpoint.updateUserPassword(session, "admin");
    }


    @Test
    public void testUpdateEmail() {
        @NotNull User user = userEndpoint.findUserById(session);
        user.setEmail("test.ru");
        @NotNull User userTest = userEndpoint.updateUserEmail(session, "newTest.ru");
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), userTest.getId());
        Assert.assertEquals(userTest.getEmail(), "newTest.ru");
    }

    @Test
    public void testUpdateLogin() {
        @NotNull User user = userEndpoint.findUserById(session);
        @NotNull User userTest = userEndpoint.updateUserLogin(session, "newLogin");
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), userTest.getId());
        Assert.assertEquals(userTest.getLogin(), "newLogin");
        userEndpoint.updateUserLogin(session, "admin");
    }

    @Test
    public void testUpdateFirstName() {
        @NotNull User user = userEndpoint.findUserById(session);
        user.setFirstName("FirstName");
        @NotNull User userTest = userEndpoint.updateUserFirstName(session, "newFirstName");
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), userTest.getId());
        Assert.assertEquals(userTest.getFirstName(), "newFirstName");
    }

    @Test
    public void testUpdateMiddleName() {
        @NotNull User user = userEndpoint.findUserById(session);
        user.setFirstName("MiddleName");
        @NotNull User userTest = userEndpoint.updateUserMiddleName(session, "newMiddleName");
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), userTest.getId());
        Assert.assertEquals(userTest.getMiddleName(), "newMiddleName");
    }

    @Test
    public void testUpdateLastName() {
        @NotNull User user = userEndpoint.findUserById(session);
        user.setFirstName("LastName");
        @NotNull User userTest = userEndpoint.updateUserLastName(session, "newLastName");
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), userTest.getId());
        Assert.assertEquals(userTest.getLastName(), "newLastName");
    }

    @Test
    public void testFindAll() {
        @NotNull List<User> users = userEndpoint.findAllUsers(session);
        Assert.assertNotNull(users);
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
    }

    @Test
    public void testFindById() {
        User user = userEndpoint.findUserById(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
        Assert.assertEquals(user.getLogin(), "admin");
    }

    @Test
    public void testFindByLogin() {
        User user = userEndpoint.findUserByLogin(session, "admin");
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
        Assert.assertEquals(user.getLogin(), "admin");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeCreateUserByLoginPasswordWithNullSession() {
        userEndpoint.createUserByLoginPassword(null, "123", "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeCreateUserByLoginPasswordWithNullLogin() {
        userEndpoint.createUserByLoginPassword(session, null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeCreateUserByLoginPasswordWithNullPassword() {
        userEndpoint.createUserByLoginPassword(session, "123", null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeCreateUserByEmailWithNullLogin() {
        userEndpoint.createUserByLoginPasswordEmail(null, "123", "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeCreateUserByEmailWithNullPassword() {
        userEndpoint.createUserByLoginPasswordEmail("login", null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeCreateUserByEmailWithNullEmail() {
        userEndpoint.createUserByLoginPasswordEmail("login", "123", null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdatePasswordWithNullSession() {
        userEndpoint.updateUserPassword(null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdatePasswordWithNullPassword() {
        userEndpoint.updateUserPassword(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdatePasswordWithEmptyPassword() {
        userEndpoint.updateUserPassword(session, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateLoginWithNullSession() {
        userEndpoint.updateUserLogin(null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateLoginWithEmptySession() {
        userEndpoint.updateUserLogin(null, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateLoginWithNullLogin() {
        userEndpoint.updateUserLogin(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateEmailWithNullSession() {
        userEndpoint.updateUserEmail(null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateEmailWithEmptySession() {
        userEndpoint.updateUserEmail(null, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateEmailWithNullPassword() {
        userEndpoint.updateUserEmail(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateFirstNameWithNullSession() {
        userEndpoint.updateUserFirstName(null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateFirstNameWithEmptyFirstName() {
        userEndpoint.updateUserFirstName(null, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateFirstNameWithNullFirstName() {
        userEndpoint.updateUserFirstName(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateMiddleNameWithNullSession() {
        userEndpoint.updateUserMiddleName(null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateMiddleNameWithEmptyMiddleName() {
        userEndpoint.updateUserMiddleName(null, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateMiddleNameWithNullMiddleName() {
        userEndpoint.updateUserMiddleName(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateLastNameWithNullSession() {
        userEndpoint.updateUserLastName(null, "123");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateLastNameWithNullLastName() {
        userEndpoint.updateUserLastName(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUpdateLastNameWithEmptyLastName() {
        userEndpoint.updateUserLastName(session, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeFindAllWithNullSession() {
        userEndpoint.findAllUsers(null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeFindByIdWithNullSession() {
        userEndpoint.findUserById(null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeFindByLoginWithNullSession() {
        userEndpoint.findUserByLogin(null, "login");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeFindByLoginWithEmptyLogin() {
        userEndpoint.findUserByLogin(null, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeFindByLoginWithNullLogin() {
        userEndpoint.findUserByLogin(session, null);
    }

}
