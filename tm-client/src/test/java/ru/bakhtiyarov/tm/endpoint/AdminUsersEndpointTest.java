package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.marker.IntegrationCategory;

import javax.xml.ws.WebServiceException;

@Category(IntegrationCategory.class)
public final class AdminUsersEndpointTest {

    @NotNull
    private static final EndpointLocator endpointLocator = new Bootstrap();

    @NotNull
    private static UserEndpoint userEndpoint;

    @NotNull
    private static AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private static SessionEndpoint sessionEndpoint;

    @NotNull
    private static Session session;

    @BeforeClass
    public static void initData() {
        userEndpoint = endpointLocator.getUserEndpoint();
        adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();
        session = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @AfterClass
    public static void closeSession() {
        sessionEndpoint.closeSessionAll(session);
    }

    @Test
    public void testRemoveUserByLogin() {
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
        userEndpoint.createUserByLoginPassword(session, "test2", "test2");
        Assert.assertEquals(3, userEndpoint.findAllUsers(session).size());
        @NotNull User user = adminUserEndpoint.removeUserByLogin(session, "test2");
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
        Assert.assertNotNull(user);
        Assert.assertEquals("test2", "test2");
    }

    @Test
    public void testRemoveUserById() {
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
        userEndpoint.createUserByLoginPassword(session, "test2", "test2");
        User newUser = userEndpoint.findUserByLogin(session, "test2");
        Assert.assertEquals(3, userEndpoint.findAllUsers(session).size());
        @NotNull User user = adminUserEndpoint.removeUserById(session, newUser.getId());
        Assert.assertEquals(2, userEndpoint.findAllUsers(session).size());
        Assert.assertNotNull(user);
        Assert.assertEquals("test2", "test2");
    }

    @Test
    public void testLockUser() {
        userEndpoint.createUserByLoginPassword(session, "test2", "test2");
        User newUser = userEndpoint.findUserByLogin(session, "test2");
        Assert.assertFalse(newUser.isLocked());
        User user = adminUserEndpoint.lockUserByLogin(session, "test2");
        Assert.assertEquals(newUser.getId(), user.getId());
        Assert.assertTrue(user.isLocked());
        adminUserEndpoint.removeUserById(session, newUser.getId());
    }

    @Test
    public void testUnLockUser() {
        userEndpoint.createUserByLoginPassword(session, "test2", "test2");
        adminUserEndpoint.lockUserByLogin(session, "test2");
        User newUser = userEndpoint.findUserByLogin(session, "test2");
        Assert.assertTrue(newUser.isLocked());
        User user = adminUserEndpoint.unLockUserByLogin(session, "test2");
        Assert.assertEquals(newUser.getId(), user.getId());
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.removeUserById(session, newUser.getId());
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeRemoveByLoginWithNullSession() {
        adminUserEndpoint.removeUserByLogin(null, "admin");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeRemoveByLoginWithNullLogin() {
        adminUserEndpoint.removeUserByLogin(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeRemoveByLoginWithEmptyLogin() {
        adminUserEndpoint.removeUserByLogin(session, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeRemoveByIdWithNullSession() {
        adminUserEndpoint.removeUserById(null, "admin");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeRemoveByIdWithNullLogin() {
        adminUserEndpoint.removeUserById(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeRemoveByIdWithEmptyLogin() {
        adminUserEndpoint.removeUserById(session, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeLockUserWithNullSession() {
        adminUserEndpoint.lockUserByLogin(null, "admin");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeLockUserWithNullLogin() {
        adminUserEndpoint.lockUserByLogin(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeLockUserWithEmptyLogin() {
        adminUserEndpoint.lockUserByLogin(session, "");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUnLockUserWithNullSession() {
        adminUserEndpoint.unLockUserByLogin(null, "admin");
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUnLockUserWithNullLogin() {
        adminUserEndpoint.unLockUserByLogin(session, null);
    }

    @Test(expected = WebServiceException.class)
    public void testNegativeUnLockUserWithEmptyLogin() {
        adminUserEndpoint.unLockUserByLogin(session, "");
    }

}
