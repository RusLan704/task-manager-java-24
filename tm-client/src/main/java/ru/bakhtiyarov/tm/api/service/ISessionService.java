package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.endpoint.Session;

public interface ISessionService {

    @NotNull
    Session getSession();

    void setSession(@Nullable Session session);

    void clearSession();

}
