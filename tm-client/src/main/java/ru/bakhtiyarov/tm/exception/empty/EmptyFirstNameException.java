package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    @NotNull
    public EmptyFirstNameException() {
        super("Error! First name is empty...");
    }

}