package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    @NotNull
    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
